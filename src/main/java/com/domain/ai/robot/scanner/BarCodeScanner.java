package com.domain.ai.robot.scanner;

import com.domain.ai.robot.common.Constants;
import org.apache.commons.lang3.StringUtils;

public class BarCodeScanner implements Scanner{

    @Override
    public double evaluatePrice(final String code) throws Exception {
        if(StringUtils.isBlank(code)){
            throw new Exception(Constants.SCAN_FAILURE_MESSAGE);
        }
        return 10.0;
    }
}
