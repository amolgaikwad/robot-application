package com.domain.ai.robot.scanner;

public interface Scanner {
    public double evaluatePrice(String code) throws Exception;
}
