package com.domain.ai.robot.common;

public class Constants {
    public static final double MAX_WEIGHT_CAN_CARRY = 10.0;
    public static final String OVERWEIGHT_MESSAGE = "OverWeight!";
    public static final double LOW_BATTERY_THRESHOLD = 15.0;
    public static final String SCAN_FAILURE_MESSAGE = "Scan Failure!";
    public static final double POWER_USED_TO_WALK_PER_KM = 20.0;
    public static final double POWER_USED_TO_CARRY_WEIGHT_PER_KG = 2.0;
}
