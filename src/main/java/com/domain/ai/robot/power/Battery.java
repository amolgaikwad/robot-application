package com.domain.ai.robot.power;

import java.util.ArrayList;
import java.util.List;

public class Battery extends PowerSource {

    protected List<PowerObserver> observers = new ArrayList<PowerObserver>();

    public Battery() {
        this.capacity = 100.0;
    }

    @Override
    public void discharge(final double power) {
        capacity = capacity - power;
        capacityChanged();
        if (capacity < 0.0) {
            capacity = 0.0;
        }
    }

    @Override
    public void charge() {
        capacity = 100.0;
        capacityChanged();
    }

    @Override
    public void registerObserver(final PowerObserver powerObserver) {
        observers.add(powerObserver);
    }

    @Override
    public void removeObserver(final PowerObserver powerObserver) {
        int index = observers.indexOf(powerObserver);
        if (index >= 0) {
            observers.remove(index);
        }
    }

    @Override
    public void notifyObservers() {
        for (PowerObserver powerObserver : observers) {
            powerObserver.updateStatus(this);
        }
    }

    public void capacityChanged() {
        notifyObservers();
    }

    public List<PowerObserver> getObservers() {
        return observers;
    }

}
