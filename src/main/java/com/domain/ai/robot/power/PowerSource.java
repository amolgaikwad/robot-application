package com.domain.ai.robot.power;

public abstract class PowerSource {
    protected double capacity;

    public double getCapacity() {
        return capacity;
    }

    public abstract void discharge(double power);

    public abstract void charge();

    public abstract void registerObserver(PowerObserver powerObserver);

    public abstract void removeObserver(PowerObserver powerObserver);

    public abstract void notifyObservers();
}
