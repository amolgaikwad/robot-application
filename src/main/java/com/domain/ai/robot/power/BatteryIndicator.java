package com.domain.ai.robot.power;

import com.domain.ai.robot.common.Constants;

public class BatteryIndicator implements PowerObserver {
    private BatteryIndicatorStatus indicator;

    public void updateStatus(final PowerSource powerSource) {

        if (powerSource != null) {
            if (powerSource.getCapacity() < Constants.LOW_BATTERY_THRESHOLD) {
                indicator = BatteryIndicatorStatus.RED;
            } else {
                indicator = BatteryIndicatorStatus.GREEN;
            }
        }
    }

    public BatteryIndicatorStatus getIndicator() {
        return indicator;
    }
}
