package com.domain.ai.robot.power;


public interface PowerObserver {
    public void updateStatus(PowerSource powerSource);
}
