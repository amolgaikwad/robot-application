package com.domain.ai.robot.power;

public enum BatteryIndicatorStatus {
    RED("red"), GREEN("green");


    private final String status;

    BatteryIndicatorStatus(final String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }
}
