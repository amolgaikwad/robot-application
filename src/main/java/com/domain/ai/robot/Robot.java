package com.domain.ai.robot;

import com.domain.ai.robot.behaviour.Action;
import com.domain.ai.robot.power.PowerSource;
import com.domain.ai.robot.scanner.Scanner;
import org.apache.commons.lang3.StringUtils;

public class Robot {

    private PowerSource powerSource;
    private Action action;

    private Scanner scanner;

    private String displayMessage;

    public Robot(PowerSource powerSource, Action action, Scanner scanner) {
        this.powerSource = powerSource;
        this.action = action;
        this.scanner = scanner;
        displayMessage = StringUtils.EMPTY;
    }

    public void performAction() {
        try {
            powerSource.discharge(action.powerUsedToPerformAction());
        } catch (Exception e) {
            displayMessage = e.getMessage();
        }
    }


    public void scan(final String barCode) {
        try {
            displayMessage = "Price is - " + scanner.evaluatePrice(barCode);
        } catch (Exception e) {
            displayMessage = e.getMessage();
        }
    }

    public PowerSource getPowerSource() {
        return powerSource;
    }

    public String getDisplayMessage() {
        return displayMessage;
    }

    public Action getAction() {
        return action;
    }
}
