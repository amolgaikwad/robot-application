package com.domain.ai.robot.behaviour;

public class Action {
    protected String actionDescription = "Unknown actionDescription";

    protected double input;

    public void setInput(final double input) {
        this.input = input;
    }

    public String getActionDescription() {
        return actionDescription;
    }

    public double powerUsedToPerformAction() throws Exception {
        return 0.0;
    }
}
