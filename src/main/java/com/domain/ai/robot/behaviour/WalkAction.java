package com.domain.ai.robot.behaviour;


import com.domain.ai.robot.common.Constants;

public class WalkAction extends ActionDecorator {

    private Action action;
    private static final String ACTION = "Walk";

    public WalkAction() {
        actionDescription = ACTION;
        action = new Action();
    }

    public WalkAction(final Action action) {
        this.actionDescription = ACTION;
        this.action = action;
    }

    @Override
    public double powerUsedToPerformAction() throws Exception {
        System.out.println("Action performed - " + getActionDescription());
        return action.powerUsedToPerformAction() + (decoratorInput * Constants.POWER_USED_TO_WALK_PER_KM);
    }
}
