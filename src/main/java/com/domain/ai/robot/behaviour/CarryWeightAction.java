package com.domain.ai.robot.behaviour;

import com.domain.ai.robot.common.Constants;

public class CarryWeightAction extends ActionDecorator {

    private Action action;
    private static final String ACTION = "Carry Weight";

    public CarryWeightAction(final Action action) {
        this.actionDescription = ACTION;
        this.action = action;
    }

    public CarryWeightAction() {
        actionDescription = ACTION;
        action = new Action();
    }

    @Override
    public double powerUsedToPerformAction() throws Exception {
        if (decoratorInput < Constants.MAX_WEIGHT_CAN_CARRY) {
            System.out.println("Action performed - " + getActionDescription());
            return action.powerUsedToPerformAction() + (decoratorInput * Constants.POWER_USED_TO_CARRY_WEIGHT_PER_KG);
        } else {
            throw new Exception(Constants.OVERWEIGHT_MESSAGE);
        }
    }
}
