package com.domain.ai.robot.behaviour;


public abstract class ActionDecorator extends Action {
    protected String action = "Unknown actionDescription";

    public void setInput(final double input) {
        this.decoratorInput = input;
    }

    protected double decoratorInput;

    public String getActionDescription() {
        return action;
    }

    public abstract double powerUsedToPerformAction() throws Exception;
}
