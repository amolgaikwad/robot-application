package com.domain.ai.robot.power;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class BatteryIndicatorTest {

    private PowerSource givenBattery;
    private BatteryIndicator batteryIndicator;

    @Before
    public void setUp() throws Exception {
        givenBattery = new Battery();
        batteryIndicator = new BatteryIndicator();
    }

    @Test
    public void shouldIndicateBatteryAsLowIfBatteryCapacityLessThan15Percent() {
        givenBatteryWithCapacity(10);
        batteryIndicator.updateStatus(givenBattery);

        assertEquals(BatteryIndicatorStatus.RED, batteryIndicator.getIndicator());
    }

    @Test
    public void shouldNotIndicateBatteryAsLowIfBatteryCapacityIsMoreThan15Percent() {
        givenBatteryWithCapacity(50);
        batteryIndicator.updateStatus(givenBattery);
        assertEquals(BatteryIndicatorStatus.GREEN, batteryIndicator.getIndicator());
    }

    @Test
    public void shouldReturnEmptyStringWhenFetchIndicatorForNullObject() {
        givenBattery = null;
        batteryIndicator.updateStatus(givenBattery);
        assertNull(batteryIndicator.getIndicator());
    }

    private void givenBatteryWithCapacity(double capacity) {
        givenBattery.discharge(100.0 - capacity);
    }

}