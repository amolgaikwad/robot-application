package com.domain.ai.robot.power;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class BatteryTest {

    private PowerSource battery;
    private PowerObserver givenPowerObserver;

    @Before
    public void setUp() {
        battery = new Battery();
    }

    @Test
    public void shouldDischargeBatteryByGivenPower() {
        double givenDischargePower = 10.0;

        whenDischargeBatteryBy(givenDischargePower);

        thenVerifyBatteryCapacityIs(90.0);
    }


    @Test
    public void shouldShowZeroCapacityIfBatteryIsDrainedCompletely() {
        double givenDischargePower = 100.0;

        whenDischargeBatteryBy(givenDischargePower);

        thenVerifyBatteryCapacityIs(0.0);
    }


    @Test
    public void shouldShowZeroCapacityIfBatteryDischargedWithPowerMoreThanCapacity() {
        Double givenDischargePower = 150.0;

        whenDischargeBatteryBy(givenDischargePower);

        thenVerifyBatteryCapacityIs(0.0);
    }

    @Test
    public void shouldChargeBatteryToFullCapacity() {
        givenBatteryDischargedBy10Percent();

        whenChargeBattery();

        thenVerifyBatteryCapacityIs(100.0);
    }

    @Test
    public void shouldRegisterPowerObserverAsObserver() {
        givenPowerObserver = new BatteryIndicator();

        whenRegisterObserver();

        thenVerifyObserverIsRegistered();
    }

    @Test
    public void shouldRemovePowerObserverFromObservers() {
        shouldRegisterPowerObserverAsObserver();
        whenRemoveObserver();
        thenVerifyObserverIsRemovedSuccessfully();
    }

    @Test
    public void shouldNotifyAllObserversIfCapacityStateChanges() {
        givenPowerObserverIsRegistered();

        whenPowerObserverUpdatesStatus();

        assertEquals(BatteryIndicatorStatus.GREEN, ((BatteryIndicator) givenPowerObserver).getIndicator());

        battery.discharge(90.0);

        assertEquals(BatteryIndicatorStatus.RED, ((BatteryIndicator) givenPowerObserver).getIndicator());
    }

    private void whenRemoveObserver() {
        battery.removeObserver(givenPowerObserver);
    }

    private void thenVerifyObserverIsRemovedSuccessfully() {
        assertTrue(((Battery) battery).getObservers().isEmpty());
    }

    private void thenVerifyObserverIsRegistered() {
        assertFalse(((Battery) battery).getObservers().isEmpty());
    }

    private void whenRegisterObserver() {
        battery.registerObserver(givenPowerObserver);
    }

    private void whenPowerObserverUpdatesStatus() {
        givenPowerObserver.updateStatus(battery);
    }

    private void givenPowerObserverIsRegistered() {
        givenPowerObserver = new BatteryIndicator();
        whenRegisterObserver();
    }

    private void whenChargeBattery() {
        battery.charge();
    }

    private void givenBatteryDischargedBy10Percent() {
        Double givenDischargePower = 10.0;

        whenDischargeBatteryBy(givenDischargePower);

        Double expectedCapacity = 90.0;
        thenVerifyBatteryCapacityIs(expectedCapacity);
    }

    private void thenVerifyBatteryCapacityIs(double expectedCapacity) {
        assertEquals(Double.valueOf(expectedCapacity), Double.valueOf(battery.getCapacity()));
    }

    private void whenDischargeBatteryBy(Double givenDischargePower) {
        battery.discharge(givenDischargePower);
    }
}
