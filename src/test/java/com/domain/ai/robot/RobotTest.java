package com.domain.ai.robot;

import com.domain.ai.robot.behaviour.Action;
import com.domain.ai.robot.behaviour.CarryWeightAction;
import com.domain.ai.robot.behaviour.WalkAction;
import com.domain.ai.robot.common.Constants;
import com.domain.ai.robot.power.Battery;
import com.domain.ai.robot.power.BatteryIndicator;
import com.domain.ai.robot.power.BatteryIndicatorStatus;
import com.domain.ai.robot.power.PowerSource;
import com.domain.ai.robot.scanner.BarCodeScanner;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class RobotTest {

    private PowerSource powerSource;
    private Robot givenRobot;
    private BatteryIndicator batteryIndicator;
    private Action action;
    private Action carryWeightWhileWalkingAction;

    @Before
    public void setUp() {
        powerSource = new Battery();
        batteryIndicator = new BatteryIndicator();
        powerSource.registerObserver(batteryIndicator);
    }

    @Test
    public void shouldIndicateBatteryStatusAsGreenIfBatteryIsFull() {
        givenRobot = new Robot(powerSource, null, null);
        whenRobotBatteryIsChargedFull();
        thenVerifyBatteryIndicatorIs(BatteryIndicatorStatus.GREEN);
    }

    @Test
    public void shouldIndicateBatteryStatusAsRedIfBatteryIsLessThanLowBatteryThreshold() {
        givenRobot = new Robot(powerSource, null, null);
        whenRobotBatteryDischargesBy(90.0);
        thenVerifyBatteryIndicatorIs(BatteryIndicatorStatus.RED);
    }

    @Test
    public void shouldDrainFullBatteryIfRobotWalksForMaxDistanceItCan() throws Exception {
        double distanceInKm = 5.0;
        givenRobotCanWalk();

        whenRobotWalksFor(distanceInKm);

        thenVerifyBatteryIndicatorIs(BatteryIndicatorStatus.RED);
        thenVerifyRobotCapacityIs(0.0);

    }

    @Test
    public void shouldVerifyBatteryCapacityIfRobotWalksForGivenDistance() throws Exception {
        double distanceInKm = 3.5;
        givenRobotCanWalk();

        whenRobotWalksFor(distanceInKm);

        thenVerifyBatteryIndicatorIs(BatteryIndicatorStatus.GREEN);
        thenVerifyRobotCapacityIs(30.0);
    }

    @Test
    public void shouldVerifyBatteryCapacityAfterCarryObjectWeighing10Kg() throws Exception {
        double givenObjectWeightInKg = 2.0;
        givenRobotCanCarryWeight();

        whenRobotCarries(givenObjectWeightInKg);

        thenVerifyBatteryIndicatorIs(BatteryIndicatorStatus.GREEN);
        thenVerifyRobotCapacityIs(96.0);
    }

    @Test
    public void shouldVerifyBatteryCapacityWhenRobotWalksForGivenDistanceWhileCarryingGivenWeight() throws Exception {
        double givenDistanceInKm = 2.0;
        double givenObjectWeightInKg = 3.0;

        givenRobotCanCarryWeightWhileWalking();

        whenRobotWalksWithWeight(givenDistanceInKm, givenObjectWeightInKg);

        thenVerifyBatteryIndicatorIs(BatteryIndicatorStatus.GREEN);
        thenVerifyRobotCapacityIs(54.0);
    }

    @Test
    public void shouldDisplayOverWeightMessageIfWeightMoreThanAllowedWeight() throws Exception {
        double givenObjectWeightInKg = 12.0;
        givenRobotCanCarryWeight();

        whenRobotCarries(givenObjectWeightInKg);

        thenVerifyOverWeightMessageIsDisplayed();
        thenVerifyRobotCapacityIs(100.0);
    }

    @Test
    public void shouldDisplayPriceAfterScanningGivenValidBarCode() {
        String givenBarCode = "12345678";
        givenRobotWithScanner();
        whenScan(givenBarCode);
        thenVerifyMessageDisplayedIs("Price is - 10.0");
    }

    @Test
    public void shouldDisplayMessageAsScanFailureWhileScanningInvalidGivenBarCode() {
        String givenBarCode = null;
        givenRobotWithScanner();

        whenScan(givenBarCode);

        thenVerifyMessageDisplayedIs(Constants.SCAN_FAILURE_MESSAGE);
    }

    private void thenVerifyMessageDisplayedIs(String expected) {
        assertEquals(expected, givenRobot.getDisplayMessage());
    }

    private void whenScan(String givenBarCode) {
        givenRobot.scan(givenBarCode);
    }

    private void givenRobotWithScanner() {
        givenRobot = new Robot(null, null, new BarCodeScanner());
    }

    private void thenVerifyBatteryIndicatorIs(BatteryIndicatorStatus expectedIndicatorStatus) {
        assertEquals(expectedIndicatorStatus, batteryIndicator.getIndicator());
    }

    private void thenVerifyOverWeightMessageIsDisplayed() {
        assertTrue(givenRobot.getDisplayMessage().equals(Constants.OVERWEIGHT_MESSAGE));
    }

    private void whenRobotBatteryIsChargedFull() {
        givenRobot.getPowerSource().charge();
    }

    private void whenRobotBatteryDischargesBy(double power) {
        powerSource.discharge(power);
    }

    private void thenVerifyRobotCapacityIs(Double expected) {
        assertEquals(expected, givenRobot.getPowerSource().getCapacity(), 0.1);
    }

    private void whenRobotWalksFor(double distance) {
        givenRobot.getAction().setInput(distance);
        givenRobot.performAction();
    }

    private void givenRobotCanWalk() {
        action = new WalkAction();
        givenRobot = new Robot(powerSource, action, null);
    }

    private void whenRobotCarries(double givenObjectWeight) {
        action.setInput(givenObjectWeight);
        givenRobot.performAction();
    }

    private void givenRobotCanCarryWeight() {
        action = new CarryWeightAction();
        givenRobot = new Robot(powerSource, action, null);
    }

    private void whenRobotWalksWithWeight(double givenDistanceFactor, double givenObjectWeightFactor) {
        carryWeightWhileWalkingAction.setInput(givenObjectWeightFactor);
        action.setInput(givenDistanceFactor);

        givenRobot.performAction();
    }

    private void givenRobotCanCarryWeightWhileWalking() {
        action = new WalkAction();

        carryWeightWhileWalkingAction = new CarryWeightAction(action);
        givenRobot = new Robot(powerSource, carryWeightWhileWalkingAction, null);
    }
}