package com.domain.ai.robot.scanner;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class BarCodeScannerTest {

    private Scanner scanner;

    @Before
    public void setUp() throws Exception {
        scanner = new BarCodeScanner();
    }

    @Test
    public void shouldEvaluatePriceForGivenValidBarCode() throws Exception {
        String givenBarCode = "12345678";
        double actualPrice = scanner.evaluatePrice(givenBarCode);
        Assert.assertEquals(10.0, actualPrice, 0.1);
    }

    @Test(expected = Exception.class)
    public void shouldThrowExceptionWhileEvaluatePriceForGivenInvalidBarCode() throws Exception {
        String givenBarCode = null;
        double actualPrice = scanner.evaluatePrice(givenBarCode);
    }

}